//= require jquery
//= require handlebars.runtime
//= require select2
//= require_tree ./templates

$(function () {
  var Wwfy = {};
  Wwfy.bindFormSelect2 = function (element) {
    var $element = $(element);

    if ($element.is('select')) {
      $element.select2({
        minimumResultsForSearch: -1,
        triggerChange: true
      });
    } else {
      var isMultiple = !!$element.data('multiple'),
          isTaggable = !!$element.data('tags'),
          isAddable = !!$element.data('canadd'),
          withImage = !!$element.data('withimage'),
          minLength = parseInt($element.data('min-length'));
      if (isNaN(minLength)) minLength = 2;
      var options = {
        multiple: isMultiple,
        minimumInputLength: minLength,
        maximumSelectionSize: 15,
        ajax: {
          url: $element.data('url'),
          dataType: 'json',
          cache: true, // not work
          quietMillis: 500,
          data: function (term, page) {
            return { term: term };
          },
          results: function (data, page) {
            return {
              results: data.results
            };
          }
        },
        initSelection: function (element, callback) {
          var results = $element.data('results'),
              value = $element.val();
          if ($.isArray(results)) {
            results = $.map(results, function (result) { return { id: result[0], text: result[1] } });
          }
          callback(results);
        },
        createSearchChoice: function(term, data) {
          if ((isAddable) && ($(data).filter( function() { return this.text.localeCompare(term)===0;
                        }).length===0)) {
            return {id:term, text:term};
          }
        }
      };
      if (isTaggable) {
        options.tags = isTaggable;
      }
      $element.select2(options); // .select2('val', []);
    }

    $element.on('change', function (event) {
      if (withImage) {
        $('#member-avatar-1').attr('src', event.added.avatar).css({ display: 'block' });
      }
    });
    $element.on('select2-removed', function (event) {
      $element.val( $element.val().replace(event.val , '') );
    });
  };

  $('.select2-input').each(function () {
    Wwfy.bindFormSelect2(this);
  });

});