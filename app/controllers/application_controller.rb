class ApplicationController < ActionController::Base
  include UrlHelper


  protect_from_forgery with: :exception
  layout :set_layout

  before_action :current_user
  before_action :current_city, if: -> { current_user.present? && current_user.city.blank? }
  before_action :menu_items


  def current_user
    @current_user = Authentication.where(auth_token: cookies[:auth_token]).first if cookies[:auth_token].present?
    @current_user
  end

  helper_method :current_user

  def current_city
    if Rails.env == 'development'
      current_user.update city: request.location.data[:city]
    else
      current_user.update city: 'Riga'
    end
  end

  def menu_items(*items)
    @menu_items = ''
  end

  def set_layout
    request.xhr? ? 'empty' : 'application'
  end

end
