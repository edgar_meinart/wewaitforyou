class CitiesController < ApplicationController
  before_action :get_city, only: [ :show, :edit, :update ]


  def show
    @proposals = @city.proposals.order(votes_count: :desc)
  end

  def edit
  end

  def update
    if @city.update(city_params)
      redirect_to @city
    else
      render action: :edit
    end
  end

  def search
    @city = City.order(name: :asc).where('LOWER(name) LIKE ?', "%#{params[:term].downcase}%").limit(20)
    render json: { results: @city.map { |object| { id: object.id, text: object.name } } }
  end

  private

    def get_city
      @city = City.where(permalink: params[:id].downcase).first
    end


    def city_params
      params.require(:city).permit(:name, :cover)
    end

end
