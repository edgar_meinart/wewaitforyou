class HomeController < ApplicationController
  layout 'landing'

  def index
    p request.subdomain
    @proposal = Proposal.new( city_id: @current_city )
  end

  def search

  end

  def find
    objects = Artist.order(name: :asc).where('LOWER(name) LIKE ? OR LOWER(permalink) LIKE ?', "%#{params[:search][:term].downcase.strip}%", "%#{params[:search][:term].downcase.strip}%").limit(20)
    cities = City.order(name: :asc).where('LOWER(name) LIKE ? OR LOWER(permalink) LIKE ?', "%#{params[:search][:term].downcase.strip}%", "%#{params[:search][:term].downcase.strip}%").limit(20)
    objects += cities
    render json: { results: objects.map { |object| { permalink: object.permalink, text: object.name, cover: object.cover.url } } }
  end

end
