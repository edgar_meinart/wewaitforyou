class ArtistsController < ApplicationController
  before_action :get_artist, only: [ :show, :edit, :update ]


  def show

  end

  def edit

  end

  def update
    if @artist.update(artist_params)
      redirect_to @artist
    else
      render action: :edit
    end
  end

  def search
    @artist = Artist.order(name: :asc).where('LOWER(name) LIKE ?', "%#{params[:term].downcase}%").limit(20)
    render json: { results: @artist.map { |object| { id: object.id, text: object.name } } }
  end

  private

    def get_artist
      value = request.subdomain.present? ? request.subdomain : params[:id]
      @artist = Artist.where(permalink: value.downcase).first
    end

    def artist_params
      params.require(:artist).permit(:name, :cover)
    end

end
