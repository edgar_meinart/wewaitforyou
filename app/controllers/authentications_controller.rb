class AuthenticationsController < ApplicationController
  def create
    @authentication = Authentication.where(uid: request.env['omniauth.auth']['uid']).first
    if @authentication.present?
      cookies[:auth_token] = @authentication.auth_token
    else
      @authentication = Authentication.create_from_omniauth(request.env['omniauth.auth'])
      cookies[:auth_token] = @authentication.auth_token
    end

    if session[:proposal_id].present?
      proposal = Proposal.where(id: session[:proposal_id]).first
      proposal.votes << Vote.create(authentication: @authentication)
      redirect_to city_proposal_path( proposal.city, proposal )
    else
      redirect_to city_path(id: 'riga')
    end
  end

end
