class ProposalsController < ApplicationController
  before_action :redirect_if_city_missing, except: [ :create ]
  before_action :get_proposal, only: [ :show ]

  def show
    @votes = @proposal.votes
  end

  def create
    #TODO rebuild!
    @porposal = Proposal.where(city_id: proposals_params[:city_id])
  end



  private

    def redirect_if_city_missing
      unless params[:city_id].present?
        redirect_to root_path(subdomain: false)
      end
    end

    def get_proposal
      @proposal = City.where(permalink: params[:city_id].to_url).first.proposals.where(permalink: request.subdomain).first
    end

    def proposals_params
      params.require(:proposal).permit :city_id, :artist_id
    end

end
