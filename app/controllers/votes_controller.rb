class VotesController < ApplicationController
  before_action :load_proposal, only: [ :new ]

  def new
    session[:proposal_id] = @proposal.id
    redirect_to '/auth/facebook'
  end

  private

    def load_proposal
      @proposal = Proposal.where(permalink: params[:proposal_id]).first
    end

end
