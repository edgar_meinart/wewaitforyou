class Vote < ActiveRecord::Base
  belongs_to :proposal, counter_cache: true

  belongs_to :authentication # as User
end
