class Artist < ActiveRecord::Base
  has_many :profile_cities, as: :profile, inverse_of: :profile, dependent: :destroy
  has_many :cities, through: :profile_cities

  has_many :profile_proposals, as: :profile, inverse_of: :profile, dependent: :destroy
  has_many :proposals, through: :profile_proposals

  acts_as_url :name, url_attribute: :permalink

  # belongs_to :proposal

  validates :name, uniqueness: true, presence: true


  has_attached_file :cover,
    :default_url => "/missing.png",
    path: ':rails_root/public/uploads/artist-covers/:id.:style.png',
    url: '/uploads/artist-covers/:id.:style.png'

  def to_param
    permalink
  end
end
