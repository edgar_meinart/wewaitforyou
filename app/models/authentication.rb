class Authentication < ActiveRecord::Base

  has_many :votes, dependent: :destroy

  has_attached_file :avatar, :styles => { :medium => "300x300#", :thumb => "100x100#" },
    :default_url => "missing.jpeg",
    path: ':rails_root/public/uploads/logos/:id.:style.png',
    url: '/uploads/logos/:id.:style.png'


  before_create :generate_auth_token

  def self.create_from_omniauth(omniauth)
    self.new.tap do |authentication|
      authentication.provider = omniauth['provider']
      authentication.uid = omniauth['uid']
      authentication.name = omniauth['info']['name']
      authentication.avatar = omniauth['info']['image']
      authentication.facebook_city = omniauth['info']['location']

      authentication.token = omniauth['credentials']['token']
      authentication.secret = omniauth['credentials']['secret']

      authentication.save
    end
  end

  def generate_auth_token
    if self.auth_token.blank?
      self.auth_token = SecureRandom.hex(12)
      generate_auth_token if Authentication.where(auth_token: self.auth_token).present?
    end
  end


end
