class Proposal < ActiveRecord::Base
  attr_accessor :artist_id
  # has_one :artist
  has_many :votes, dependent: :destroy
  has_many :voted_users, through: :votes, source: :authentication

  has_one :profile_proposal, inverse_of: :proposal, dependent: :destroy
  has_one :artist, through: :profile_proposal, source: :profile, source_type: 'Artist'

  acts_as_url :artist_name, url_attribute: :permalink

  belongs_to :city

  # validates :artist_id, presence: true

  def artist_name
    artist.name
  end

  def to_param
    permalink
  end

  def find_or_create

  end


end
