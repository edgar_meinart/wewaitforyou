class City < ActiveRecord::Base

  has_many :profile_cities, inverse_of: :city, dependent: :destroy
  has_many :artists, through: :profile_cities, source: :profile, source_type: 'Artist'

  has_many :proposals

  belongs_to :country

  acts_as_url :name, url_attribute: :permalink

  has_attached_file :cover,
    :default_url => "/missing.png",
    path: ':rails_root/public/uploads/city-covers/:id.:style.png',
    url: '/uploads/city-covers/:id.:style.png'

  def to_param
    permalink
  end

end
