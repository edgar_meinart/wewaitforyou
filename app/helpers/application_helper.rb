module ApplicationHelper

  def set_menu_items(*items)
    html = ""
    items.each do |link|
      html << "<li> #{link_to( link[0], link[1] )} </li>"
    end
    @menu_items = html
  end
end
