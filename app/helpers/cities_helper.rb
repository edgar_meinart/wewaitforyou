module CitiesHelper
  def city_cover_photo_for(city)
    return city.cover.url if city.cover.present?
    return image_url("cities/#{city.permalink}/cover#{rand(1)+1}.jpg")
  end
end
