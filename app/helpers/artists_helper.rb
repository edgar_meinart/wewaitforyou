module ArtistsHelper
  def artist_cover_photo_for(artist)
    return artist.cover.url if artist.cover.present?
    return image_url("backgrounds/artists/artist#{rand(3)+1}.jpg")
  end

end
