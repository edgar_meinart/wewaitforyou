require 'csv'

# Importing countries from a CSV file
# Country.delete_all
puts 'Countries'
countries = Country.pluck(:abbreviation)
ActiveRecord::Base.transaction do
  CSV.foreach(Rails.root.join('db', 'countryInfo.txt'), col_sep: "\t") do |row|
    unless countries.include?(row[0])
      c = Country.create name: row[4], abbreviation: row[0]
      print c.new_record? ? '-' : '+'
    end
  end
end
puts ''
puts 'Done!'

# Importing world cities from a CSV file
countries = Country.all.group_by(&:abbreviation)
# City.delete_all
puts 'Cities'
cities = City.pluck(:country_id, :name).map { |c| "#{c[0]}-#{c[1]}" }
ActiveRecord::Base.transaction do
  CSV.foreach(Rails.root.join('db', 'cities15000.txt'), col_sep: "\t") do |row|
    unless cities.include?("#{countries[row[8]].first.id}-#{row[2]}")
      c = City.create name: row[2], country_id: countries[row[8]].first.id, lat: row[4], lng: row[5]
      print c.new_record? ? '-' : '+'
    end
  end
end
puts ''
puts 'Done!'


file = File.open(Rails.root.join('db', 'artists.txt'), "r")
artists = file.read
file.close
count = 0;
artists.each_line do |line|
  artist = Artist.new(name:  line.gsub("\n", ''))
  count += 1
  if artist.save
    puts "#{artist.name} count"
  else
    puts ' - '
  end
end

puts ''
puts 'Done!'
