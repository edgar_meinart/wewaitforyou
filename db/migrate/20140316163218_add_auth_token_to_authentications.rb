class AddAuthTokenToAuthentications < ActiveRecord::Migration
  def change
    add_column :authentications, :auth_token, :string
    add_index :authentications, :auth_token, unique: true
  end
end
