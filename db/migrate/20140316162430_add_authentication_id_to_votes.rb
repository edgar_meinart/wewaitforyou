class AddAuthenticationIdToVotes < ActiveRecord::Migration
  def change
    add_column :votes, :authentication_id, :integer
  end
end
