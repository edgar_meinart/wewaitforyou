class CreateProposals < ActiveRecord::Migration
  def change
    create_table :proposals do |t|
      t.integer :city_id
      t.timestamps
    end
  end
end
