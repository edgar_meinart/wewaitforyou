class AddAttachmentAvatarToAuthentications < ActiveRecord::Migration
  def self.up
    change_table :authentications do |t|
      t.attachment :avatar
    end
  end

  def self.down
    drop_attached_file :authentications, :avatar
  end
end
