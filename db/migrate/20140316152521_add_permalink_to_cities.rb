class AddPermalinkToCities < ActiveRecord::Migration
  def change
    add_column :cities, :permalink, :string
    add_index :cities, :permalink, unique: true
  end
end
