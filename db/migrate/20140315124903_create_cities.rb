class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name
      t.integer :country_id
      t.decimal :lat
      t.decimal :lng

      t.timestamps
    end
    add_index :cities, :name
    add_index :cities, :country_id
  end
end
