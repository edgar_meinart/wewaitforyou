class AddPermalinkToProposals < ActiveRecord::Migration
  def change
    add_column :proposals, :permalink, :string
    add_index :proposals, :permalink, unique: true
  end
end
