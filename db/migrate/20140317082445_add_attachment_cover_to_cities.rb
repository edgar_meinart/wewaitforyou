class AddAttachmentCoverToCities < ActiveRecord::Migration
  def self.up
    change_table :cities do |t|
      t.attachment :cover
    end
  end

  def self.down
    drop_attached_file :cities, :cover
  end
end
