class AddPermalinkToArtists < ActiveRecord::Migration
  def change
    add_column :artists, :permalink, :string
    add_index :artists, :permalink, unique: true
  end
end
