class AddUidIndexToAuthentications < ActiveRecord::Migration
  def change
    add_index :authentications, :uid, unique: true
  end
end
