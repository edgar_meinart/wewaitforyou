class RemoveVoteIdFromAuthentications < ActiveRecord::Migration
  def up
    remove_column :authentications, :vote_id
  end

  def down
    add_column :authentications, :vote_id, :integer
  end
end
