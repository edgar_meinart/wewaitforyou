class AddCityToAuthentications < ActiveRecord::Migration
  def change
    add_column :authentications, :city, :string
    add_column :authentications, :facebook_city, :string
  end
end
