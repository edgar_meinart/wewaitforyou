class CreateProfileCities < ActiveRecord::Migration
  def change
    create_table :profile_cities do |t|
      t.references :city, index: true
      t.integer :profile_id
      t.string :profile_type
      t.timestamps
    end
  end
end
