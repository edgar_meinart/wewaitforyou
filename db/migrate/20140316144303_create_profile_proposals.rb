class CreateProfileProposals < ActiveRecord::Migration
  def change
    create_table :profile_proposals do |t|
      t.string :profile_type
      t.integer :profile_id
      t.references :proposal, index: true
      t.timestamps
    end
  end
end
