# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140317141407) do

  create_table "artists", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "permalink"
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
  end

  add_index "artists", ["name"], name: "index_artists_on_name"
  add_index "artists", ["permalink"], name: "index_artists_on_permalink", unique: true

  create_table "authentications", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "token"
    t.string   "secret"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "auth_token"
    t.string   "city"
    t.string   "facebook_city"
  end

  add_index "authentications", ["auth_token"], name: "index_authentications_on_auth_token", unique: true
  add_index "authentications", ["uid"], name: "index_authentications_on_uid", unique: true

  create_table "cities", force: true do |t|
    t.string   "name"
    t.integer  "country_id"
    t.decimal  "lat"
    t.decimal  "lng"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "permalink"
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
  end

  add_index "cities", ["country_id"], name: "index_cities_on_country_id"
  add_index "cities", ["name"], name: "index_cities_on_name"
  add_index "cities", ["permalink"], name: "index_cities_on_permalink", unique: true

  create_table "countries", force: true do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "profile_cities", force: true do |t|
    t.integer  "city_id"
    t.integer  "profile_id"
    t.string   "profile_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "profile_cities", ["city_id"], name: "index_profile_cities_on_city_id"

  create_table "profile_proposals", force: true do |t|
    t.string   "profile_type"
    t.integer  "profile_id"
    t.integer  "proposal_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "profile_proposals", ["proposal_id"], name: "index_profile_proposals_on_proposal_id"

  create_table "proposals", force: true do |t|
    t.integer  "city_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "permalink"
    t.integer  "votes_count", default: 0
  end

  add_index "proposals", ["permalink"], name: "index_proposals_on_permalink", unique: true

  create_table "votes", force: true do |t|
    t.integer  "proposal_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "authentication_id"
  end

end
