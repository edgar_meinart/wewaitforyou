Wewaitforyouin::Application.routes.draw do
  root to: 'home#index'

  get '/auth/:provider/callback', to: 'authentications#create'

  resource :home, only: [ :index ], path: '/', controller: 'home' do
    get :search
    post :find
  end

  resources :cities, only: [ :show ], path: '/' do
    constraints(Subdomain) do
      get '/' => 'proposals#show'
    end
    resources :proposals, only: [ :show ], path: '/proposals' do
      resources :votes, only: [ :new ]
    end
  end
  resources :cities, excepy: [ :edit, :update, :show, :new, :create, :index, :destroy ] do
    get 'search', on: :collection
  end
  resources :proposals, only: [ :create ]
  resources :artists, only: [ :create ] do
    get 'search', on: :collection
  end

  scope :admin do
    resources :cities, only: [ :edit, :update, :show ]
    resources :artists, only: [ :edit, :update, :show ]
  end

end
